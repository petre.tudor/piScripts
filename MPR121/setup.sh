#!/usr/bin/env bash

echo "Updating OS..."
sudo apt-get update
echo "Installing dependencies"
sudo apt-get install build-essential python-dev python-smbus python-pip git

echo "Cloning MPR121 library..."
cd ~
git clone https://github.com/adafruit/Adafruit_Python_MPR121.git

echo "Installing MPR121..."
cd Adafruit_Python_MPR121
sudo python setup.py install

echo "Creating desktop shortcut for simpletest..."

cat > simpletest.desktop <<EOF
[Desktop Entry]
Version=1.0
Name=SimpleTestMPR121
Comment=Just testing a setup script
Exec=bash -c 'sudo python ~/Adafruit_Python_MPR121/examples/simpletest.py;$SHELL'
Icon=utilities-terminal
Terminal=true
Type=Application
Categories=Utility;Application;
EOF

echo "Moving .desktop file to ~/.local/share/applications"
mv simpletest.desktop ~/.local/share/applications/

echo "done"
